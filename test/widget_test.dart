// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:calculo_imc/main.dart';

void main() {
  Map<String, Finder> getFinders() {
    Finder pesoFinder = find.byKey(new Key('peso'));
    Finder alturaFinder = find.byKey(new Key('altura'));
    Finder formWidgetFinder = find.byType(Form);
    return {
      'peso': pesoFinder,
      'altura': alturaFinder,
      'formWidgetFinder': formWidgetFinder
    };
  }

  void widgetImc(Map<String, dynamic> parameters) async {
    expect(find.text(parameters['infoText']), findsOneWidget);
    await parameters['tester']
        .enterText(parameters['finders']['peso'], parameters['peso']);
    await parameters['tester']
        .enterText(parameters['finders']['altura'], parameters['altura']);
    await parameters['tester'].pump();

    Form formWidget = parameters['tester']
        .widget(parameters['finders']['formWidgetFinder']) as Form;
    GlobalKey<FormState> formKey = formWidget.key as GlobalKey<FormState>;

    expect(formKey.currentState.validate(), isTrue);

    await parameters['tester'].tap(find.byKey(Key('calcular')));
    await parameters['tester'].pump();

    expect(find.text(parameters['result']), findsOneWidget);
  }

  group('Campos válidos e Pesos', () {
    testWidgets('Abaixo do peso', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(home: Home()));

      Map<String, dynamic> parameters = {
        'tester': tester,
        'finders': getFinders(),
        'infoText': "Informe seus dados!",
        'result': "Abaixo do peso (15.6)",
        'peso': "50",
        'altura': "179"
      };

      widgetImc(parameters);
    });

    testWidgets('Peso ideal', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(home: Home()));

      Map<String, dynamic> parameters = {
        'tester': tester,
        'finders': getFinders(),
        'infoText': "Informe seus dados!",
        'result': "Peso ideal (18.7)",
        'peso': "60",
        'altura': "179"
      };

      widgetImc(parameters);
    });

    testWidgets('Levemente acima do peso', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(home: Home()));

      Map<String, dynamic> parameters = {
        'tester': tester,
        'finders': getFinders(),
        'infoText': "Informe seus dados!",
        'result': "Levemente Acima (25.0)",
        'peso': "80",
        'altura': "179"
      };

      widgetImc(parameters);
    });

    testWidgets('Obesidade Grau I', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(home: Home()));

      Map<String, dynamic> parameters = {
        'tester': tester,
        'finders': getFinders(),
        'infoText': "Informe seus dados!",
        'result': "Obesidade Grau I (31.2)",
        'peso': "100",
        'altura': "179"
      };

      widgetImc(parameters);
    });

    testWidgets('Obesidade Grau II', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(home: Home()));

      Map<String, dynamic> parameters = {
        'tester': tester,
        'finders': getFinders(),
        'infoText': "Informe seus dados!",
        'result': "Obesidade Grau II (37.5)",
        'peso': "120",
        'altura': "179"
      };

      widgetImc(parameters);
    });

    testWidgets('Obesidade Grau III', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(home: Home()));

      Map<String, dynamic> parameters = {
        'tester': tester,
        'finders': getFinders(),
        'infoText': "Informe seus dados!",
        'result': "Obesidade Grau III (40.6)",
        'peso': "130",
        'altura': "179"
      };

      widgetImc(parameters);
    });
  });

  testWidgets('Campos inválidos', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(home: Home()));

    Finder formWidgetFinder = getFinders()['formWidgetFinder'];
    Form formWidget = tester.widget(formWidgetFinder) as Form;
    GlobalKey<FormState> formKey = formWidget.key as GlobalKey<FormState>;

    expect(formKey.currentState.validate(), isFalse);
  });
}
